import time

import googleapiclient.discovery

from ..common import CloudTester


class GCECloudTester(CloudTester):
    def __init__(self, project, zone, image_name):
        super().__init__()
        self.project = project
        self.zone = zone
        self.compute = None
        self.credentials = None
        self.test_instance = 'cloud-tester'
        self.image_name = image_name

    def connect(self):
        super().connect()
        self.compute = googleapiclient.discovery.build('compute', 'v1')

    def _wait_for_operation(self, operation):
        while True:
            time.sleep(5)
            result = self.compute.zoneOperations().get(
                project=self.project,
                zone=self.zone,
                operation=operation['name']).execute()
    
            if result['status'] == 'DONE':
                if 'error' in result:
                    raise Exception(result['error'])
                return result

    def start_image(self):
        super().start_image()
        # TODO: put image we want to test here
        image_response = self.compute.images().get(
            project=self.project, image=self.image_name).execute()
        source_disk_image = image_response['selfLink']

        # Configure the machine
        machine_type = "zones/%s/machineTypes/n1-standard-1" % self.zone

        config = {
            'name': self.test_instance,
            'machineType': machine_type,

            # Specify the boot disk and the image to use as a source.
            'disks': [
                {
                    'boot': True,
                    'autoDelete': True,
                    'initializeParams': {
                        'sourceImage': source_disk_image,
                    }
                }
            ],

            # Specify a network interface with NAT to access the public
            # internet.
            'networkInterfaces': [{
                'network': 'global/networks/default',
                'accessConfigs': [
                    {'type': 'ONE_TO_ONE_NAT', 'name': 'External NAT'}
                ]
            }],
            "metadata": {
                "items": [
                    {
                        "key": "ssh-keys",
                        "value": "admin:%s" % self.pub_key,
                    },
                    {
                        "key": "block-project-ssh-keys",
                        "value": "TRUE"
                    }
                ]
            },
        }

        operation = self.compute.instances().insert(
            project=self.project,
            zone=self.zone,
            body=config).execute()
        self._wait_for_operation(operation)

        result = self.compute.instances().get(project=self.project, zone=self.zone, instance=self.test_instance).execute()
        self.ip = result['networkInterfaces'][0]['accessConfigs'][0]['natIP']
        self.connection = self.connect_to_machine(self.ip, 'admin')

    def stop_image(self):
        super().stop_image()
        operation = self.compute.instances().delete(
            project=self.project,
            zone=self.zone,
            instance=self.test_instance).execute()
        self._wait_for_operation(operation)
